/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package io.asouquieres.avromodel;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

/** Quotation model */
@org.apache.avro.specific.AvroGenerated
public class Quotation extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -4986542461458820073L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"Quotation\",\"namespace\":\"io.asouquieres.avromodel\",\"doc\":\"Quotation model\",\"fields\":[{\"name\":\"orderId\",\"type\":\"long\"},{\"name\":\"customerId\",\"type\":\"int\"},{\"name\":\"total\",\"type\":\"int\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<Quotation> ENCODER =
      new BinaryMessageEncoder<Quotation>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<Quotation> DECODER =
      new BinaryMessageDecoder<Quotation>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<Quotation> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<Quotation> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<Quotation> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<Quotation>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this Quotation to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a Quotation from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a Quotation instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static Quotation fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private long orderId;
   private int customerId;
   private int total;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public Quotation() {}

  /**
   * All-args constructor.
   * @param orderId The new value for orderId
   * @param customerId The new value for customerId
   * @param total The new value for total
   */
  public Quotation(java.lang.Long orderId, java.lang.Integer customerId, java.lang.Integer total) {
    this.orderId = orderId;
    this.customerId = customerId;
    this.total = total;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return orderId;
    case 1: return customerId;
    case 2: return total;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: orderId = (java.lang.Long)value$; break;
    case 1: customerId = (java.lang.Integer)value$; break;
    case 2: total = (java.lang.Integer)value$; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'orderId' field.
   * @return The value of the 'orderId' field.
   */
  public long getOrderId() {
    return orderId;
  }


  /**
   * Sets the value of the 'orderId' field.
   * @param value the value to set.
   */
  public void setOrderId(long value) {
    this.orderId = value;
  }

  /**
   * Gets the value of the 'customerId' field.
   * @return The value of the 'customerId' field.
   */
  public int getCustomerId() {
    return customerId;
  }


  /**
   * Sets the value of the 'customerId' field.
   * @param value the value to set.
   */
  public void setCustomerId(int value) {
    this.customerId = value;
  }

  /**
   * Gets the value of the 'total' field.
   * @return The value of the 'total' field.
   */
  public int getTotal() {
    return total;
  }


  /**
   * Sets the value of the 'total' field.
   * @param value the value to set.
   */
  public void setTotal(int value) {
    this.total = value;
  }

  /**
   * Creates a new Quotation RecordBuilder.
   * @return A new Quotation RecordBuilder
   */
  public static io.asouquieres.avromodel.Quotation.Builder newBuilder() {
    return new io.asouquieres.avromodel.Quotation.Builder();
  }

  /**
   * Creates a new Quotation RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new Quotation RecordBuilder
   */
  public static io.asouquieres.avromodel.Quotation.Builder newBuilder(io.asouquieres.avromodel.Quotation.Builder other) {
    if (other == null) {
      return new io.asouquieres.avromodel.Quotation.Builder();
    } else {
      return new io.asouquieres.avromodel.Quotation.Builder(other);
    }
  }

  /**
   * Creates a new Quotation RecordBuilder by copying an existing Quotation instance.
   * @param other The existing instance to copy.
   * @return A new Quotation RecordBuilder
   */
  public static io.asouquieres.avromodel.Quotation.Builder newBuilder(io.asouquieres.avromodel.Quotation other) {
    if (other == null) {
      return new io.asouquieres.avromodel.Quotation.Builder();
    } else {
      return new io.asouquieres.avromodel.Quotation.Builder(other);
    }
  }

  /**
   * RecordBuilder for Quotation instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<Quotation>
    implements org.apache.avro.data.RecordBuilder<Quotation> {

    private long orderId;
    private int customerId;
    private int total;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(io.asouquieres.avromodel.Quotation.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.orderId)) {
        this.orderId = data().deepCopy(fields()[0].schema(), other.orderId);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.customerId)) {
        this.customerId = data().deepCopy(fields()[1].schema(), other.customerId);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (isValidValue(fields()[2], other.total)) {
        this.total = data().deepCopy(fields()[2].schema(), other.total);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
    }

    /**
     * Creates a Builder by copying an existing Quotation instance
     * @param other The existing instance to copy.
     */
    private Builder(io.asouquieres.avromodel.Quotation other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.orderId)) {
        this.orderId = data().deepCopy(fields()[0].schema(), other.orderId);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.customerId)) {
        this.customerId = data().deepCopy(fields()[1].schema(), other.customerId);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.total)) {
        this.total = data().deepCopy(fields()[2].schema(), other.total);
        fieldSetFlags()[2] = true;
      }
    }

    /**
      * Gets the value of the 'orderId' field.
      * @return The value.
      */
    public long getOrderId() {
      return orderId;
    }


    /**
      * Sets the value of the 'orderId' field.
      * @param value The value of 'orderId'.
      * @return This builder.
      */
    public io.asouquieres.avromodel.Quotation.Builder setOrderId(long value) {
      validate(fields()[0], value);
      this.orderId = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'orderId' field has been set.
      * @return True if the 'orderId' field has been set, false otherwise.
      */
    public boolean hasOrderId() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'orderId' field.
      * @return This builder.
      */
    public io.asouquieres.avromodel.Quotation.Builder clearOrderId() {
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'customerId' field.
      * @return The value.
      */
    public int getCustomerId() {
      return customerId;
    }


    /**
      * Sets the value of the 'customerId' field.
      * @param value The value of 'customerId'.
      * @return This builder.
      */
    public io.asouquieres.avromodel.Quotation.Builder setCustomerId(int value) {
      validate(fields()[1], value);
      this.customerId = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'customerId' field has been set.
      * @return True if the 'customerId' field has been set, false otherwise.
      */
    public boolean hasCustomerId() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'customerId' field.
      * @return This builder.
      */
    public io.asouquieres.avromodel.Quotation.Builder clearCustomerId() {
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'total' field.
      * @return The value.
      */
    public int getTotal() {
      return total;
    }


    /**
      * Sets the value of the 'total' field.
      * @param value The value of 'total'.
      * @return This builder.
      */
    public io.asouquieres.avromodel.Quotation.Builder setTotal(int value) {
      validate(fields()[2], value);
      this.total = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'total' field has been set.
      * @return True if the 'total' field has been set, false otherwise.
      */
    public boolean hasTotal() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'total' field.
      * @return This builder.
      */
    public io.asouquieres.avromodel.Quotation.Builder clearTotal() {
      fieldSetFlags()[2] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Quotation build() {
      try {
        Quotation record = new Quotation();
        record.orderId = fieldSetFlags()[0] ? this.orderId : (java.lang.Long) defaultValue(fields()[0]);
        record.customerId = fieldSetFlags()[1] ? this.customerId : (java.lang.Integer) defaultValue(fields()[1]);
        record.total = fieldSetFlags()[2] ? this.total : (java.lang.Integer) defaultValue(fields()[2]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<Quotation>
    WRITER$ = (org.apache.avro.io.DatumWriter<Quotation>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<Quotation>
    READER$ = (org.apache.avro.io.DatumReader<Quotation>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeLong(this.orderId);

    out.writeInt(this.customerId);

    out.writeInt(this.total);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.orderId = in.readLong();

      this.customerId = in.readInt();

      this.total = in.readInt();

    } else {
      for (int i = 0; i < 3; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.orderId = in.readLong();
          break;

        case 1:
          this.customerId = in.readInt();
          break;

        case 2:
          this.total = in.readInt();
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










