package io.asouquieres.kstream.stream.error;

public class StreamWithErrorConstants {

    public static final String RESULT_TOPIC = "ResultTopic";
    public static final String SOURCE_TOPIC = "SourceTopic";

    public static final String DLQ_TOPIC_NAME = "DlqTopic";

}
