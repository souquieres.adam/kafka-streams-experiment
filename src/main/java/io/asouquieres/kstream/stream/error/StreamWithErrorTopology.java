package io.asouquieres.kstream.stream.error;

import io.asouquieres.kstream.helpers.MayBeException;
import io.asouquieres.kstream.helpers.StreamException;
import io.asouquieres.kstream.helpers.StreamExceptionCatcher;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;

/**
 * This topology will survive any exception inside code logic thanks to StreamExceptionCatcher
 */
public class StreamWithErrorTopology {

    public static Topology getTopology() {

        StreamsBuilder builder = new StreamsBuilder();

        var sourceStream = builder.stream(StreamWithErrorConstants.SOURCE_TOPIC, Consumed.with(Serdes.String(), Serdes.Integer()));


        var canThrowError = sourceStream.map( (k,v) -> {
            try {

                if(v == 5) {
                    throw new IllegalStateException("Unexpected issue during stream processing");
                }

                var result = v*10;

                return KeyValue.pair(k, MayBeException.of(result));

            } catch (Exception e) {
                return KeyValue.pair(k, MayBeException.of(StreamException.of(e, v)));
            }
        });

       StreamExceptionCatcher.handle(canThrowError)
               .to(StreamWithErrorConstants.RESULT_TOPIC, Produced.with(Serdes.String(), Serdes.Integer()));

        return builder.build();
    }
}
