package io.asouquieres.kstream.stream.error;

import io.asouquieres.kstream.helpers.PropertiesLoader;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.log4j.Logger;


public class StreamWithErrorLauncherApp {

    private static final Logger logger = Logger.getLogger(StreamWithErrorLauncherApp.class);

    public static void main(String[] args) {

        // Get stream configuration
        var streamsConfiguration = PropertiesLoader.fromYaml("application.yml");

        //Build topology
        var stream = new KafkaStreams(StreamWithErrorTopology.getTopology(), streamsConfiguration);

        // Define handler in case of unmanaged exception
        stream.setUncaughtExceptionHandler( (thread, e) -> {
            logger.fatal("Exception interrupted the stream", e);
            logger.fatal("Closing all threads for " + streamsConfiguration.get(StreamsConfig.APPLICATION_ID_CONFIG));
            stream.close();
        });
        // Start stream execution
        stream.start();

        // Ensure your app respond gracefully to external shutdown signal
        Runtime.getRuntime().addShutdownHook(new Thread(stream::close));
    }
}
