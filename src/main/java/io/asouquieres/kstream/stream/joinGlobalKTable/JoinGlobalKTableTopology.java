package io.asouquieres.kstream.stream.joinGlobalKTable;

import io.asouquieres.avromodel.*;
import io.asouquieres.kstream.helpers.AvroSerdes;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;

import static io.asouquieres.kstream.stream.joinGlobalKTable.JoinGlobalKTableConstants.QUOTATION;

public class JoinGlobalKTableTopology {

    public static Topology getTopology() {

        StreamsBuilder builder = new StreamsBuilder();

        var globalKTable =
                builder.globalTable(JoinGlobalKTableConstants.CUSTOMER_REF,
                        Consumed.with(Serdes.Long(),
                                AvroSerdes.<Customer>get()));

        var sourceStream = builder.stream(JoinGlobalKTableConstants.ORDERS,
                Consumed.with(Serdes.String(), AvroSerdes.<Order>get()));

        // No need for stream rekey : a global ktable is replicated on each stream thread
        sourceStream
                .join(globalKTable,  (k, v) -> v.getCustomerId(), JoinGlobalKTableTopology::buildQuotation)
                .to(JoinGlobalKTableConstants.QUOTATION, Produced.with(Serdes.String(), AvroSerdes.<Quotation>get()));

        return builder.build();
    }

    private static Quotation buildQuotation(Order o, Customer c) {
        // Any business logic here
        return Quotation.newBuilder().build();
    }
}
