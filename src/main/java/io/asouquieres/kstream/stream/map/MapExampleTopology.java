package io.asouquieres.kstream.stream.map;

import io.asouquieres.avromodel.Customer;
import io.asouquieres.avromodel.LegacyCustomer;
import io.asouquieres.kstream.helpers.AvroSerdes;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;

public class MapExampleTopology {

    public static Topology getTopology() {

        StreamsBuilder builder = new StreamsBuilder();

        var sourceStream = builder.stream(MapExampleConstants.SOURCE_TOPIC, Consumed.with(Serdes.Long(), AvroSerdes.<Customer>get()));

        sourceStream.map( (k, v) -> KeyValue.pair(buildNewKey(k), convertValue(v)))
                .to(MapExampleConstants.TARGET_TOPIC, Produced.with(Serdes.Long(), AvroSerdes.<LegacyCustomer>get()));

        return builder.build();
    }


    private static LegacyCustomer convertValue(Customer r) {
        return LegacyCustomer.newBuilder()
                .setIdentifier(r.getId())
                .setCName(r.getCommonName())
                .setContactDate(r.getInscriptionDate())
                .setFullAddress( r.getCommonName() + " "+r.getPostalCode()+ " "+r.getAdress())
                .build();
    }

    private static Long buildNewKey(Long k) {
        return k+1000000;
    }
}
