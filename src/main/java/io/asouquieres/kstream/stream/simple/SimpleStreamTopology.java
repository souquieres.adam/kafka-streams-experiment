package io.asouquieres.kstream.stream.simple;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;

public class SimpleStreamTopology {

    public static Topology getTopology() {

        StreamsBuilder builder = new StreamsBuilder();

        var sourceStream = builder.stream(SimpleStreamConstants.SOURCE_TOPIC, Consumed.with(Serdes.String(), Serdes.Integer()));

        sourceStream.filter((k, v) -> v > 10)
                .to(SimpleStreamConstants.FILTERED_TOPIC, Produced.with(Serdes.String(), Serdes.Integer()));

        return builder.build();
    }
}
