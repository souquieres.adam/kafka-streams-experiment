package io.asouquieres.kstream.stream.simple;

public class SimpleStreamConstants {

    public static String SOURCE_TOPIC = "SourceTopic";
    public static String FILTERED_TOPIC = "FilteredTopic";

}
