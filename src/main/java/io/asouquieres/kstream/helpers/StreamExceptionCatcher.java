package io.asouquieres.kstream.helpers;


import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

public class StreamExceptionCatcher {

    public static String DLQ_NAME = "dlq.topic";
    /**
     *
     */
    public static <SK, SV> KStream<SK, SV> handle(KStream<SK, MayBeException<SV>> stream) {

        var branches = stream.branch(
                (k, v) -> v.streamException != null, //0 Error output
                (k, v) -> true ); //1 Standard output

        branches[0].map( (k,v) -> KeyValue.pair(k.toString(), v.streamException.toString()))
                .to(StreamContext.getProps().getProperty(DLQ_NAME), Produced.with(Serdes.String(), Serdes.String()));

        return branches[1].mapValues( v -> v.streamValue);
    }
}
