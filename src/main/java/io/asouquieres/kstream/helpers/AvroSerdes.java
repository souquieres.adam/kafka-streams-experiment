package io.asouquieres.kstream.helpers;

import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.avro.specific.SpecificRecord;

import java.util.Map;

public class AvroSerdes {

    public static final String SCHEMA_REGISTRY_URL = "schema.registry.url";

    public static <T extends SpecificRecord> SpecificAvroSerde<T> get() {
        var specificSerdes = new SpecificAvroSerde<T>();
        specificSerdes.configure(Map.of(SCHEMA_REGISTRY_URL, StreamContext.getProps().get(SCHEMA_REGISTRY_URL)), false);
        return specificSerdes;
    }
}
