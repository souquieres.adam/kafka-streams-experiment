package io.asouquieres.kstream;

import io.asouquieres.kstream.stream.simple.SimpleStreamConstants;
import io.asouquieres.kstream.stream.simple.SimpleStreamTopology;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleStreamTopologyTest {
    private TestInputTopic<String, Integer> inputTopic;
    private TestOutputTopic<String, Integer> outputTopic;

    @BeforeEach
    public void init() {

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");

        TopologyTestDriver testDriver = new TopologyTestDriver(SimpleStreamTopology.getTopology(), props, Instant.now());

        testDriver.advanceWallClockTime(Duration.ofMinutes(10));

        testDriver.getStateStore("");

        inputTopic = testDriver.createInputTopic(SimpleStreamConstants.SOURCE_TOPIC, Serdes.String().serializer(), Serdes.Integer().serializer());
        outputTopic = testDriver.createOutputTopic(SimpleStreamConstants.FILTERED_TOPIC, Serdes.String().deserializer(), Serdes.Integer().deserializer());
    }

    @Test
    public void shouldFilterValuesLowerThan10() {

        for (int i = 0; i < 100; i++) {
            inputTopic.pipeInput(""+i, i);
        }

        var outputList =outputTopic.readValuesToList();

        assertEquals(89, outputList.size());
    }
}
