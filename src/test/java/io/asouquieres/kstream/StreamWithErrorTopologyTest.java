package io.asouquieres.kstream;

import io.asouquieres.kstream.helpers.StreamContext;
import io.asouquieres.kstream.stream.error.StreamWithErrorConstants;
import io.asouquieres.kstream.stream.error.StreamWithErrorTopology;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Properties;

import static io.asouquieres.kstream.helpers.StreamExceptionCatcher.DLQ_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class StreamWithErrorTopologyTest {
    private TestInputTopic<String, Integer> inputTopic;
    private TestOutputTopic<String, Integer> outputTopic;


    private TestOutputTopic<String, String> dlqTopic;


    @BeforeEach
    public void init() {

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        props.put(DLQ_NAME, StreamWithErrorConstants.DLQ_TOPIC_NAME);

        StreamContext.setProps(props);
        TopologyTestDriver testDriver = new TopologyTestDriver(StreamWithErrorTopology.getTopology(), props);

        inputTopic = testDriver.createInputTopic(StreamWithErrorConstants.SOURCE_TOPIC, Serdes.String().serializer(), Serdes.Integer().serializer());
        outputTopic = testDriver.createOutputTopic(StreamWithErrorConstants.RESULT_TOPIC, Serdes.String().deserializer(), Serdes.Integer().deserializer());

        dlqTopic = testDriver.createOutputTopic(StreamWithErrorConstants.DLQ_TOPIC_NAME, Serdes.String().deserializer(), Serdes.String().deserializer());

    }

    @Test
    public void shouldProduceALlInputValuesAndADlqMessage() {

        for (int i = 0; i < 10; i++) {
            inputTopic.pipeInput(""+i, i);
        }

        var outputList =outputTopic.readValuesToList();
        var outputDlqList =dlqTopic.readValuesToList();

        assertEquals(List.of(0, 10, 20, 30, 40, 60, 70, 80, 90), outputList);
        assertEquals(List.of("StreamException{innerException=Unexpected issue during stream processing, failingValue=5}"), outputDlqList);
    }
}
