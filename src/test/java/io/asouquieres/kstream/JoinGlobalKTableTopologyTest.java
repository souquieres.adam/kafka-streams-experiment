package io.asouquieres.kstream;

import io.asouquieres.avromodel.ChildAvro;
import io.asouquieres.avromodel.DemoAvro;
import io.asouquieres.kstream.helpers.AvroSerdes;
import io.asouquieres.kstream.helpers.StreamContext;
import io.asouquieres.kstream.stream.simpleavro.SimpleStreamWithAvroConstants;
import io.asouquieres.kstream.stream.simpleavro.JoinGlobalKTableTopology;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JoinGlobalKTableTopologyTest {
    private TestInputTopic<String, DemoAvro> inputTopic;
    private TestOutputTopic<String, DemoAvro> outputTopic;

    @BeforeEach
    public void init() {

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        props.put(AvroSerdes.SCHEMA_REGISTRY_URL, "mock://dummy:1234");
        StreamContext.setProps(props);
        TopologyTestDriver testDriver = new TopologyTestDriver(JoinGlobalKTableTopology.getTopology(), props);

        inputTopic = testDriver.createInputTopic(SimpleStreamWithAvroConstants.SOURCE_TOPIC, Serdes.String().serializer(), AvroSerdes.<DemoAvro>get().serializer());
        outputTopic = testDriver.createOutputTopic(SimpleStreamWithAvroConstants.FILTERED_TOPIC, Serdes.String().deserializer(), AvroSerdes.<DemoAvro>get().deserializer());
    }

    @Test
    public void shouldFilterValuesLowerThan10() {

        for (int i = 0; i < 100; i++) {
            inputTopic.pipeInput(""+i, buildDemoAvro(i));
        }

        var outputList =outputTopic.readValuesToList();

        assertEquals(89, outputList.size());
    }

    private DemoAvro buildDemoAvro(long longValue) {
        return DemoAvro.newBuilder()
                .setArrayOfObject(List.of(ChildAvro.newBuilder().setName("childA").build()))
                .setLongField(longValue)
                .setBooleanField(true)
                .setBytesField(ByteBuffer.allocate(10))
                .setDecimalField(new BigDecimal(5).setScale(2, RoundingMode.DOWN))
                .setDoubleField(16)
                .setFloatField(12)
                .setMapOfReusedObjects(Map.of("v1", ChildAvro.newBuilder().setName("child1").build()))
                .setMapOfStrings(Map.of("key","value"))
                .setOptionalStringField(null)
                .setStringField("anyString")
                .setTimestampLogicalField(Instant.now())
                .setIntField(4)
                .build();
    }
}
